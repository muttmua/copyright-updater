class Changelog

  attr_accessor :changeset, :author, :email, :date, :year, :summary, :files

  def initialize()
    @changeset = ""
    @author = ""
    @email = ""
    @date = ""
    @year = 0
    @summary = ""
    @files = Array.new
  end

  # this accepts a block and invokes it for each parse changelog
  def Changelog.each_entry(pipe)
    entry = Changelog.new()
    in_files = false

    pipe.each() do |line|
      line.chomp!

      case line
      when /^$/
        yield entry
        entry = Changelog.new()
        in_files = false
        next
      when /^changeset: (.*)$/
        entry.changeset = $1
      when /^author: (.*)$/
        entry.author = $1
        if entry.author =~ /<(.*)>/
          entry.email = $1
        else
          entry.email = entry.author
        end
      when /^date: (.*)$/
        entry.date = $1
        if entry.date =~ /^(\d+)-/
          entry.year = Integer($1, 10)
        else
          raise "Illegal date #{entry.date}"
        end
      when /^summary: (.*)$/
        entry.summary = $1
      when /^files:$/
        in_files = true
      else
        if in_files
          entry.files.push line
        end
      end
    end
  end
end
