require_relative "year_range"

class Author
  @@authors = Hash.new

  def initialize(name, email)
    @name = name
    @emails = Array.new
    @emails.push(email.downcase)
  end

  def name
    @name
  end

  def emails
    @emails
  end

  def add_email(email)
    @emails.push(email.downcase)
  end

  def has_email(email)
    @emails.include?(email.downcase)
  end

  def Author.clear_authors()
    @@authors = Hash.new
  end

  def Author.authors()
    @@authors.values().uniq()
  end

  def Author.add_author(author)
    author.emails.each do |email|
      @@authors[email.downcase] = author
    end
  end

  def Author.get_author(email)
    return(@@authors[email.downcase])
  end

  def Author.has_author(email)
    return(@@authors.has_key?(email.downcase))
  end

end
