require_relative "year_range"
require_relative "author"

MAX_SCAN_LINES = 40

class SourceFile
  @@source_files = Hash.new

  def initialize(base_dir, filename)
    if (! base_dir.end_with?("/"))
      base_dir = "#{base_dir}/"
    end
    @base_dir = base_dir
    @filename = filename
    @line_to_copyright = Hash.new
    @author_to_copyright = Hash.new
  end

  def filename
    @filename
  end

  def path
    "#{@base_dir}/#{@filename}"
  end

  def author_copyright(author)
    @author_to_copyright[author]
  end

  def line_copyright(line)
    @line_to_copyright[line]
  end

  def authors
    @author_to_copyright.keys()
  end

  def lines
    @line_to_copyright.keys()
  end

  def copyrights
    @line_to_copyright.values()
  end

  def modified?()
    @author_to_copyright.values().each() do |copyright|
      return 1 if copyright.modified?()
    end
    return nil
  end

  def parse_file()
    line_no = 0
    # This is not super robust.  Should probably deal
    # with the file not existing and the exception
    # if copyright.parse blows up.
    File.open("#{@base_dir}/#{@filename}") do |file|
      file.each_line do |line|
        line_no += 1
        break if line_no > MAX_SCAN_LINES
        # This does not deal with multi-line copyrights, like in md5.h.
        # Fortunately that isn't ours so it doesn't matter.
        # Also, this regexp isn't great.  A more accurate year capture
        # might be (\d+(\s*[,-]\s*)*\d+) but for now the current
        # version is adequate
        next if line !~ /^.*copyright\s+\(c\)\s+[-,\d\s]+[[:alpha:]]/i

        copyright = Copyright.new
        copyright.parse(line.chomp())
        @line_to_copyright[line_no] = copyright
        @author_to_copyright[copyright.author()] = copyright
      end
    end
  end

  # This is passed in a file object, and writes
  # the current (updated) file to that object
  def output_file(outfile)
    line_no = 0
    File.open("#{@base_dir}/#{@filename}") do |infile|
      infile.each_line do |inline|
        line_no += 1
        if (line_no <= MAX_SCAN_LINES)
          copyright = @line_to_copyright[line_no]
          if copyright && copyright.modified?()
            inline = copyright.to_s()
          end
        end

        # Note: puts adds a newline if needed
        outfile.puts(inline)
      end
    end
  end

  # Temp version for the 4-digit conversion.
  # Update *all* copyrights
  def force_output_file(outfile)
    line_no = 0
    File.open("#{@base_dir}/#{@filename}") do |infile|
      infile.each_line do |inline|
        line_no += 1
        if (line_no <= MAX_SCAN_LINES)
          copyright = @line_to_copyright[line_no]
          if copyright
            inline = copyright.to_s()
          end
        end

        # Note: puts adds a newline if needed
        outfile.puts(inline)
      end
    end
  end

  def SourceFile.source_files()
    @@source_files.values()
  end

  def SourceFile.clear_files()
    @@source_files = Hash.new
  end

  def SourceFile.add_file(file)
    @@source_files[file.filename()] = file
  end

  def SourceFile.get_file(filename)
    return(@@source_files[filename])
  end

  def SourceFile.has_file(filename)
    return(@@source_files.has_key?(filename))
  end

end
