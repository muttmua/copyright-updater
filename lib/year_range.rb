class YearRange
  def initialize
    @years = Hash.new
    @modified = nil
  end

  def parse(year_string)
    pieces = year_string.split(/\s*,\s*/)
    pieces.each do |piece|
      parse_piece(piece).each do |year|
        @years[year] = 1
      end
    end
  end

  def has_year(year)
    year = Integer(year.to_s, 10)
    return @years.has_key?(year)
  end

  def add_year(year)
    year = Integer(year.to_s, 10)
    if (! @years.has_key?(year))
      @years[year] = 1
      @modified = 1
    end
  end

  def modified?()
    return(@modified)
  end

  def to_s
    pieces = Array.new
    state = 0
    start_year = end_year = next_year = nil
    years = @years.keys.sort {|a,b| b <=> a}

    while years.size > 0
      next_year = years.pop
      case state
      when 0   # no start or end
        start_year = next_year
        state = 1
      when 1   # have start year but no end year
        if next_year == (start_year + 1)
          end_year = next_year
          state = 2
        else
          years.push(next_year)
          state = 0
          pieces.push(start_year.to_s)
        end
      when 2   # have start and end year
        if next_year == (end_year + 1)
          end_year = next_year
        else
          years.push(next_year)
          state = 0
          pieces.push(range_string(start_year, end_year))
        end
      end
    end

    case state
    when 1   # have start year but no end year
      pieces.push(start_year.to_s)
    when 2   # have start and end year
      pieces.push(range_string(start_year, end_year))
    end

    return pieces.join(",")
  end

  private

  # Returns an Array of the years contained in the piece
  def parse_piece(piece)
    if piece =~ /-/
      range_ends = piece.split(/\s*-\s*/)
      if range_ends.size != 2
        raise "parse_piece: Invalid range #{piece}"
      end
      return parse_range(range_ends[0], range_ends[1])
    else
      return [round_year(parse_integer(piece))]
    end
  end

  # Returns an Array of the years contained in the range
  # sample input: 1996-7   1999-2005   2002-4  1998-99
  def parse_range(start_range_str, end_range_str)
    start_range = round_year(parse_integer(start_range_str))
    end_range = parse_integer(end_range_str)

    if ((start_range < 1970) || (start_range > 2100))
      raise "parse_range: illegal range start #{start_range}"
    end

    [10, 100, 1000].each do |round_val|
      if (end_range < round_val)
        end_range = end_range + (start_range / round_val * round_val)
        break
      end
    end

    if (end_range < start_range)
      raise "parse_range: illegal range #{start_range}..#{end_range}"
    end

    return (start_range..end_range).to_a
  end

  def parse_integer(int_string)
    if int_string !~ /^\s*(\d+)\s*$/
      raise "invalid integer #{int_string}"
    end
    int_string = $1
    return Integer(int_string, 10)
  end

  def round_year(year)
    if year < 100
      if year < 80
        return(year + 2000)
      else
        return(year + 1900)
      end
    end

    return year
  end

  # Return a range string.
  # NOTE: although we parse ranges like 2005-512 to mean 2005-2512,
  #       we will only output single-digit second parts of the range
  def range_string(start_year, end_year)
    # According to Vincent, single-digit years in the range
    # are actually not allowed.  So just return the full version
    # for now.
    return "#{start_year}-#{end_year}"

    # if ((start_year / 10) == (end_year / 10))
    #   return "#{start_year}-#{end_year%10}"
    # else
    #   return "#{start_year}-#{end_year}"
    # end
  end
end
