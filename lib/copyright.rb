require_relative "year_range"
require_relative "author"

class Copyright
  def initialize
    @author = nil
    @years = YearRange.new
    @prefix = nil
    @suffix = nil
  end

  def author
    @author
  end

  def years
    @years
  end

  def parse(line)
    line = line.chomp
    if line !~ /^(.*copyright\s+\(c\)\s+)([-,\d\s]+)([[:alpha:]].*)$/i
      raise "Illegal copyright line: #{line}"
    end
    @prefix = $1
    year_range = $2
    @suffix = $3

    @years.parse($2)

    # Some copyright lines are missing an email address:
    # we'll just use the name in that case.
    #
    # note: .*? is lazy matching to allow the following
    #       \s* to suck up all the spaces
    if @suffix =~ /^(.*?)\s*<(.*)>/
      name = $1
      email = $2
    else
      name = email = @suffix
    end

    if Author.has_author(email)
      @author = Author.get_author(email)
    else
      @author = Author.new(name, email)
      Author.add_author(@author)
    end
  end

  def to_s
    "#{@prefix}#{@years.to_s} #{@suffix}"
  end

  # Checks if any of the copyright years have been modfied
  def modified?
    return(@years.modified?())
  end

end
