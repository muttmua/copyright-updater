require 'minitest/autorun'
require 'minitest/pride'

require_relative "../lib/copyright"
require_relative "../lib/author"

class TestCopyright < Minitest::Test
  def setup
    @copyright = Copyright.new

    @kevin = Author.new("Kevin McCarthy", "kevin@8t8.us")
    @dgc = Author.new("David Champion", "dgc@bikeshed.us")
    @dgc.add_email("dgc@mutt.org")

    Author.clear_authors()
    Author.add_author(@kevin)
    Author.add_author(@dgc)
  end

  def test_parse_simple
    input = "* Copyright (C) 2006-7 David Champion <dgc@bikeshed.us>\n"
    output = "* Copyright (C) 2006-2007 David Champion <dgc@bikeshed.us>"

    @copyright.parse(input)
    assert_equal(@dgc, @copyright.author())
    assert_equal("2006-2007", @copyright.years.to_s())
    assert_equal(output, @copyright.to_s())
  end

  def test_trailing_stuff
    input = "* Copyright (C) 2006-7 , 2010 David Champion <dgc@bikeshed.us>, and others\n"
    output = "* Copyright (C) 2006-2007,2010 David Champion <dgc@bikeshed.us>, and others"

    @copyright.parse(input)
    assert_equal(@dgc, @copyright.author())
    assert_equal("2006-2007,2010", @copyright.years.to_s())
    assert_equal(output, @copyright.to_s())
  end

  def test_update_years
    input = "* Copyright (C) 2006-7 , 2010 David Champion <dgc@mutt.org>, and others\n"
    output = "* Copyright (C) 2006-2007,2010-2011 David Champion <dgc@mutt.org>, and others"

    @copyright.parse(input)
    @copyright.years().add_year(2011)
    assert_equal(@dgc, @copyright.author())
    assert_equal("2006-2007,2010-2011", @copyright.years.to_s())
    assert_equal(output, @copyright.to_s())
  end

  def test_modified
    input = "* Copyright (C) 2006-7 David Champion <dgc@bikeshed.us>\n"

    @copyright.parse(input)
    assert(! @copyright.modified?())
    @copyright.years.add_year(2008)
    assert(@copyright.modified?())
  end

  def test_parsing_name
    input = "* Copyright (C) 1996-2000 Michael R. Elkins <me@mutt.org>\n"

    @copyright.parse(input)
    assert_equal("Michael R. Elkins", @copyright.author().name())
  end
end
