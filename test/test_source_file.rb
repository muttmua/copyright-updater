require 'minitest/autorun'
require 'minitest/pride'

require 'tempfile'

require_relative "../lib/copyright"
require_relative "../lib/author"
require_relative "../lib/source_file"

class TestSourceFile < Minitest::Test
  def setup
    Author.clear_authors()
    SourceFile.clear_files()
  end

  def test_copyright_file
    copyright_file = SourceFile.new("./testdata", "COPYRIGHT")
    copyright_file.parse_file()

    me = Author.get_author("me@cs.hmc.edu")
    assert(me)
    me_cr = copyright_file.author_copyright(me)
    assert(me_cr)
    assert_equal(me, me_cr.author())
    assert_equal(me_cr, copyright_file.line_copyright(9))
    assert(me_cr.years().has_year(2006))

    rocco = Author.get_author("pdmef@gmx.net")
    assert(rocco)
    rocco_cr = copyright_file.author_copyright(rocco)
    assert(rocco_cr)
    assert_equal(rocco, rocco_cr.author())
    assert_equal(rocco_cr, copyright_file.line_copyright(16))
    assert(rocco_cr.years().has_year(2008))

    assert_equal(8, copyright_file.authors().size())
  end

  def test_main_c()
    main_file = SourceFile.new("./testdata", "main.c")
    main_file.parse_file()

    me = Author.get_author("me@mutt.org")
    assert(me)
    me_cr = main_file.author_copyright(me)
    assert(me_cr)
    assert_equal(me, me_cr.author())
    assert_equal(me_cr, main_file.line_copyright(2))
    assert(me_cr.years().has_year(1997))

    roessler = Author.get_author("roessler@does-not-exist.org")
    assert(roessler)
    roessler_cr = main_file.author_copyright(roessler)
    assert(roessler_cr)
    assert_equal(roessler_cr, main_file.line_copyright(3))

    g10 = Author.get_author("g10 Code GmbH")
    assert(g10)
    g10_cr = main_file.author_copyright(g10)
    assert(g10_cr)
    assert_equal(g10, g10_cr.author())
    assert_equal(g10_cr, main_file.line_copyright(4))
    assert(g10_cr.years().has_year(2004))

    assert_equal(3, main_file.authors().size())
  end

  def test_unchanged_output()
    main_file = SourceFile.new("./testdata", "main.c")
    main_file.parse_file()

    temp_file = Tempfile.new("cpmainc")
    begin
      main_file.output_file(temp_file)
      temp_file.close()
      diff_out = `/usr/bin/diff ./testdata/main.c #{temp_file.path()}`
      assert_equal("", diff_out)
    ensure
      temp_file.unlink
    end
  end

  def test_update_output()
    main_file = SourceFile.new("./testdata", "main.c")
    main_file.parse_file()

    g10 = Author.get_author("g10 Code GmbH")
    assert(g10)
    g10_cr = main_file.author_copyright(g10)
    assert(g10_cr)
    g10_cr.years().add_year(2005)

    temp_file = Tempfile.new("cpmainc")
    begin
      main_file.output_file(temp_file)
      temp_file.close()
      diff_out = `/usr/bin/diff ./testdata/main.c #{temp_file.path()}`
      expected_out = <<EOS
4c4
<  * Copyright (C) 2004 g10 Code GmbH
---
>  * Copyright (C) 2004-2005 g10 Code GmbH
EOS

      assert_equal(expected_out, diff_out)
    ensure
      temp_file.unlink
    end
  end

  def test_modified()
    main_file = SourceFile.new("./testdata", "main.c")
    main_file.parse_file()
    assert(! main_file.modified?())

    g10 = Author.get_author("g10 Code GmbH")
    assert(g10)
    g10_cr = main_file.author_copyright(g10)
    assert(g10_cr)
    g10_cr.years().add_year(2005)
    assert(main_file.modified?())
  end

  # edmundo and rocco, when rewritten, will be in a different
  # format, so this also checks to make sure non-modifed copyright
  # lines are not rewritten
  def test_modify_copyright_file
    copyright_file = SourceFile.new("./testdata", "COPYRIGHT")
    copyright_file.parse_file()

    brendan = Author.get_author("brendan@kublai.com")
    assert(brendan)
    brendan_cr = copyright_file.author_copyright(brendan)
    assert(brendan_cr)
    assert_equal(brendan_cr, copyright_file.line_copyright(13))
    brendan_cr.years().add_year(1998)

    temp_file = Tempfile.new("cpmainc")
    begin
      copyright_file.output_file(temp_file)
      temp_file.close()
      diff_out = `/usr/bin/diff ./testdata/COPYRIGHT #{temp_file.path()}`
      expected_out = <<EOS
13c13
< Copyright (C) 1999-2009 Brendan Cully <brendan@kublai.com>
---
> Copyright (C) 1998-2009 Brendan Cully <brendan@kublai.com>
EOS

      assert_equal(expected_out, diff_out)
    ensure
      temp_file.unlink
    end
  end

end
