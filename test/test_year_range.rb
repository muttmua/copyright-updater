require 'minitest/autorun'
require 'minitest/pride'

require_relative '../lib/year_range'

class TestYearRange < Minitest::Test
  def setup
    @year_range = YearRange.new
  end

  def test_parse_single_year
    @year_range.parse("1999")

    assert_equal("1999", @year_range.to_s())
  end

  def test_parse_simple_year_range
    @year_range.parse("1980-1987")

    assert_equal("1980-1987", @year_range.to_s())
  end

  def test_complicated_year_range
    @year_range.parse("1981-4")

    assert_equal("1981-1984", @year_range.to_s())
  end

  def test_multiple_ranges
    @year_range.parse("1979,1980,1981,1985,1986,1987,1988")

    assert_equal("1979-1981,1985-1988", @year_range.to_s())
  end

  def test_out_of_order_ranges
    @year_range.parse("2000,2005,1991,2003,1992,2002,1993,2001")

    assert_equal("1991-1993,2000-2003,2005", @year_range.to_s())
  end

  def test_small_years_1
    @year_range.parse("1996-2000,2")

    assert_equal("1996-2000,2002", @year_range.to_s())
  end

  def test_small_years_2
    @year_range.parse("1985,89,90,91,92,93,95,96,97")

    assert_equal("1985,1989-1993,1995-1997", @year_range.to_s())
  end

  def test_small_years_3
    @year_range.parse("1992, 93, 96, 97, 98, 99, 2004")

    assert_equal("1992-1993,1996-1999,2004", @year_range.to_s())
  end

  def test_has_year
    @year_range.parse("2002-2005,1990-1995")
    assert(@year_range.has_year(2003))
    assert(@year_range.has_year("1992"))
    assert(! @year_range.has_year("2006"))

    assert_equal("1990-1995,2002-2005", @year_range.to_s())
    assert(@year_range.has_year("2005"))
  end

  def test_add_year
    @year_range.parse("2001-2010")
    assert(! @year_range.has_year(2000))

    @year_range.add_year(2000)
    assert(@year_range.has_year(2000))
    assert_equal("2000-2010", @year_range.to_s())
  end

  def test_dups
    @year_range.parse("2000,2001,2000,1999-2002")
    assert_equal("1999-2002", @year_range.to_s())
  end

  def test_spacing
    @year_range.parse("2002 - 2005 , 2007, 2009 ")
    assert_equal("2002-2005,2007,2009", @year_range.to_s())
  end

  def test_modified
    @year_range.parse("2001-2010")
    assert(! @year_range.modified?())
    @year_range.add_year(2009)
    assert(! @year_range.modified?())
    @year_range.add_year(2011)
    assert(@year_range.modified?())
  end
end
