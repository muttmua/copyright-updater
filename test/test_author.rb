require 'minitest/autorun'
require 'minitest/pride'

require_relative '../lib/author'

class TestAuthor < Minitest::Test
  def setup
    Author.clear_authors()
    @kevin = Author.new("Kevin McCarthy", "kevin@8t8.us")
    @dgc = Author.new("David Champion", "dgc@bikeshed.us")
    @dgc.add_email("dgc@mutt.org")
  end

  def test_has_email
    assert(@kevin.has_email("kevin@8t8.us"))
    assert(! @kevin.has_email("foo"))

    @kevin.add_email("foo")
    assert(@kevin.has_email("foo"))
  end

  def test_author_db
    assert(! Author.get_author("kevin@8t8.us"))
    Author.add_author(@kevin)
    assert_equal(@kevin, Author.get_author("kevin@8t8.us"))

    assert(! Author.get_author("dgc@mutt.org"))
    Author.add_author(@dgc)
    assert_equal(@dgc, Author.get_author("dgc@mutt.org"))
    assert_equal(@dgc, Author.get_author("dgc@bikeshed.us"))

    assert_equal(2, Author.authors().size())
  end

  def test_case_match
    assert(@kevin.has_email("KEVIN@8t8.us"))
    Author.add_author(@dgc)
    assert_equal(@dgc, Author.get_author("DGC@MUTT.ORG"))
  end
end
