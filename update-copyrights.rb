#!/usr/bin/ruby
#
# update-copyrights.rb
#
# Copyright (C) 2015 Kevin J. McCarthy <kevin@8t8.us>
#
# Tool to assist in updating the copyright notices in the mutt
# source code and COPYRIGHT file.
#
# invoke with:
#    ./update-copyrights.rb <repo-location> <changeset#>
# The changeset# is the number to start scanning from.  Presumably
# this would be the changeset after the last copyright update.
#

require 'tempfile'

require_relative 'lib/year_range'
require_relative 'lib/copyright'
require_relative 'lib/source_file'
require_relative 'lib/author'
require_relative 'lib/changelog'

# Globals used
$debug_log = nil
$report_log = nil
$copyright_file = nil

def open_logs()
  now = Time.now()
  timestamp = now.strftime("%Y-%m-%d_%H-%M")
  $debug_log = File.open("log/debug-#{timestamp}.log", "w")
  $report_log = File.open("log/report-#{timestamp}.log", "w")
end

# This adds authors with multiple email addresses (found in the
# sourcefile copyrights and hg commit logs).  We want to conflate them
# so we can properly update copyrights.
#
# When a copyright doesn't have an email address listed, the name is
# used as the hash/lookup key.  This is why some of the entries below
# has strange values for the "email address" parameter.
def preseed_authors()
  edmund = Author.new("Edmund Grimley Evans", "edmundo@rano.org")
  edmund.add_email("edmund820@rano.org")
  Author.add_author(edmund)

  g10 = Author.new("g10 Code GmbH", "g10 code gmbh")
  g10.add_email("g10code gmbh")
  g10.add_email("moritz@g10code.com")
  g10.add_email("Moritz.Schulte@ruhr-uni-bochum.de")
  Author.add_author(g10)

  me = Author.new("Michael R. Elkins", "me@cs.hmc.edu")
  me.add_email("me@mutt.org")
  me.add_email("me@sigpipe.org")
  me.add_email("michael r. elkins.")  # this is from filter.c
  Author.add_author(me)

  mikes = Author.new("Mike Schiraldi", "raldi@research.netsol.com")
  mikes.add_email("1074468571@schiraldi.org")
  Author.add_author(mikes)

  rocco = Author.new("Rocco Rutte", "pdmef@gmx.net")
  rocco.add_email("s1118644@mail.inf.tu-dresden.de")
  Author.add_author(rocco)

  werner = Author.new("Werner Koch", "werner.koch@guug.de")
  werner.add_email("wk@gnupg.org")
  werner.add_email("wk@isil.d.shuttle.de")
  Author.add_author(werner)
end

# This creates entries in the Author and SourceFile class
# holding all the files and authors with copyright lines
def scan_source_files(gitdir)
  ["", "imap/"].each do |suffix|
    Dir.foreach("#{gitdir}#{suffix}") do |file|
      next if file !~ /.+\.([ch]|pl)$/
      next if !File.file?("#{gitdir}#{suffix}#{file}")

      source_file = SourceFile.new(gitdir, "#{suffix}#{file}")
      source_file.parse_file()
      if source_file.lines().size() > 0
        SourceFile.add_file(source_file)
      end
    end
  end

  # We record this in a global because it will be frequently accessed.
  # Don't put it in SourceFile.add_file() because we don't want it
  # to be a file considered for updating like the *.[ch] files
  $copyright_file = SourceFile.new(gitdir, "COPYRIGHT")
  $copyright_file.parse_file()
end

def init(gitdir)
  open_logs()
  preseed_authors()
  scan_source_files(gitdir)
end

# Silly way to clear the screen, but this will make it easier
# to review things
def clear_screen()
  system("clear")
end

def confirm_changeset(entry, prompt)
  clear_screen()

  puts "\nConfirming changeset #{entry.changeset()}:"
  puts "============================================================="
  puts "Summary: #{entry.summary()}"
  puts "Files: #{entry.files().join(', ')}"
  puts "==> #{prompt}"
  puts "Are you sure you want to process this changeset?"
  answer = STDIN.gets()
  if answer =~ /^\s*y/i
    return 1
  else
    return nil
  end
end

def confirm_file_copyright_addition(entry, entry_file, gitdir)
  # when invoked from another dir, the entry_file has to
  # be prefixed with the hg directory
  args = ["git",
          "-C", gitdir,
          "log",
          "-1", "-p",
          entry.changeset(),
          "--", entry_file]

  clear_screen()

  puts "\nPotential copyright addition"
  puts "============================"
  puts "Changeset summary and diff for the file #{entry_file} follows"
  puts "Please confirm if this should result in a copyright update"
  puts "- - - - - - - - - - - - - - - - -"

  IO.popen(args) do |gitdiff|
    gitdiff.each() do |line|
      puts line
    end
  end

  puts "- - - - - - - - - - - - - - - - -"
  puts "Should this changeset add a copyright to #{entry_file}?"
  answer = STDIN.gets()
  if answer =~ /^\s*y/i
    return 1
  else
    return nil
  end
end

def process_changesets(gitdir, start_date)
  template = "changeset: %h%n" +
             "author: %an <%ae>%n" +
             "date: %cd%n" +
             "summary: %s%n" +
             "files:"
  args = ["git",
          "-C", gitdir,
          "log",
          "--all",
          "--reverse",
          "--date=short",
          "--name-only",
          "--pretty=format:#{template}",
          "--since=#{start_date}"]

  IO.popen(args) do |gitlog|
    Changelog.each_entry(gitlog) do |entry|
      $debug_log.puts("Processing changeset #{entry.changeset()}")

      author = Author.get_author(entry.email())
      next if !author  # they have no copyrights anywhere

      if (entry.summary() =~ /(spelling)/i) ||
         (entry.summary() =~ /(copyright)/i) ||
         (entry.summary() =~ /(backout|backed out|back out)/i) ||
         (entry.summary() =~ /(merge)/i)
        next if !confirm_changeset(entry, "Summary contains the word '#{$1}'")
      end

      if (entry.files().size() > 15)
        next if !confirm_changeset(entry, "Contains more than 15 files")
      end

      modified_file = nil

      entry.files().each do |entry_file|
        source_file = SourceFile.get_file(entry_file)
        next if !source_file  # it's not a *.[ch] file

        copyright = source_file.author_copyright(author)
        next if !copyright  # they don't have a copyright in this file

        next if copyright.years().has_year(entry.year())

        if (confirm_file_copyright_addition(entry, entry_file, gitdir))
          if ! modified_file
            $report_log.puts("Changeset #{entry.changeset()} author #{entry.author()}")
          end
          $report_log.puts("Adding year #{entry.year()} to #{entry_file}")
          copyright.years().add_year(entry.year())
          modified_file = 1
        end
      end

      if modified_file
        copyright_cr = $copyright_file.author_copyright(author)
        if copyright_cr && (!copyright_cr.years().has_year(entry.year()))
          $report_log.puts("Adding year #{entry.year()} to COPYRIGHT")
          copyright_cr.years().add_year(entry.year())
        end
        $report_log.puts
      end
    end
  end
end

def update_file(source_file)
  return if !source_file.modified?()

  temp_file = Tempfile.new("updcpr")
  begin
    # dump the copyright updates to the tempfile
    source_file.output_file(temp_file)
    temp_file.close()

    perms = File.stat(source_file.path()).mode()

    # copy the temp file over the orginal file, preserving permissions
    File.open(temp_file.path()) do |tempf|
      File.open(source_file.path(), 'wb', perms) do |srcf|
        IO.copy_stream(tempf, srcf)
      end
    end

  ensure
    temp_file.unlink()
  end
end

def update_files()
  SourceFile.source_files().each() do |file|
    update_file(file)
  end

  update_file($copyright_file)
end

def cleanup()
  $debug_log.close()
  $report_log.close()
end


if ARGV.length != 2
  puts "Usage #{$0} gitrepos startdate"
  puts "gitrepos is the path to the mutt git repository to update"
  puts "startdate should be the date to start scanning from"
  exit 1
end

gitdir = ARGV[0]
if (! gitdir.end_with?("/"))
  gitdir = gitdir + "/"
end
if ((! File.directory?(gitdir)) || (! File.directory?("#{gitdir}/.git")))
  puts "#{gitdir} doesn't seem to point to a mutt git repository"
  exit 1
end

start_date = ARGV[1]

init(gitdir)

process_changesets(gitdir, start_date)

update_files()

cleanup()



