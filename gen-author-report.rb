#!/usr/bin/ruby

require_relative 'lib/year_range'
require_relative 'lib/copyright'
require_relative 'lib/source_file'
require_relative 'lib/author'

# This adds authors with multiple email addresses in the file copyrights
# and hg commit logs.  We want to conflate them so we can properly update
# copyrights
def preseed_authors()
  edmund = Author.new("Edmund Grimley Evans", "edmundo@rano.org")
  edmund.add_email("edmund820@rano.org")
  Author.add_author(edmund)

  g10 = Author.new("g10 Code GmbH", "g10 code gmbh")
  g10.add_email("g10code gmbh")
  g10.add_email("moritz@g10code.com")
  g10.add_email("Moritz.Schulte@ruhr-uni-bochum.de")
  Author.add_author(g10)

  me = Author.new("Michael R. Elkins", "me@cs.hmc.edu")
  me.add_email("me@mutt.org")
  me.add_email("me@sigpipe.org")
  me.add_email("michael r. elkins.")  # this is from filter.c
  Author.add_author(me)

  mikes = Author.new("Mike Schiraldi", "raldi@research.netsol.com")
  mikes.add_email("1074468571@schiraldi.org")
  Author.add_author(mikes)

  rocco = Author.new("Rocco Rutte", "pdmef@gmx.net")
  rocco.add_email("s1118644@mail.inf.tu-dresden.de")
  Author.add_author(rocco)

  werner = Author.new("Werner Koch", "werner.koch@guug.de")
  werner.add_email("wk@gnupg.org")
  werner.add_email("wk@isil.d.shuttle.de")
  Author.add_author(werner)
end


def scan_authors(top_dir)
  ["", "imap/"].each do |suffix|
    Dir.foreach("#{top_dir}#{suffix}") do |file|
      next if file !~ /.+\.[ch]$/
      next if !File.file?("#{top_dir}#{suffix}#{file}")

      source_file = SourceFile.new(top_dir, "#{suffix}#{file}")
      source_file.parse_file()
      if source_file.lines().size() > 0
        SourceFile.add_file(source_file)
      end
    end
  end

  copyright = SourceFile.new(top_dir, "COPYRIGHT")
  copyright.parse_file()
end

def gen_author_report_1()
  authors = Author.authors().sort() {|a,b| a.name() <=> b.name()}
  authors.each() do |author|
    puts "*#{author.name()}*:"
    author.emails().each() do |email|
      puts "  <#{email}>"
    end
  end
end

def gen_author_report_2()
  authors = Author.authors().sort() {|a,b| a.name() <=> b.name()}
  authors.each() do |author|
    author.emails().each() do |email|
      puts "#{author.name()} <#{email}>"
    end
  end
end

# Generate in the same format as grep so it will be easy to
# check against
def gen_file_report()
  SourceFile.source_files().each() do |file|
    file.lines().each() do |line|
      copyright = file.line_copyright(line)
      puts "#{file.filename()}:#{line}:#{copyright.to_s()}"
    end
  end
end

preseed_authors()

scan_authors("/home/kjm/projects/mutt/devel/local/cright/")

# gen_author_report_1()
gen_author_report_2()

# gen_file_report()
