#!/usr/bin/ruby
#
# update-copyrights.rb
#
# Copyright (C) 2015 Kevin J. McCarthy <kevin@8t8.us>
#
# This is a one time script to convert *all* copyright notices
# to use 4-digit years.
#
# invoke with:
#    ./update-copyrights.rb <repo-location>
#

require 'tempfile'

require_relative 'lib/year_range'
require_relative 'lib/copyright'
require_relative 'lib/source_file'
require_relative 'lib/author'
require_relative 'lib/changelog'

# Globals used
$debug_log = nil
$report_log = nil
$copyright_file = nil

def open_logs()
  now = Time.now()
  timestamp = now.strftime("%Y-%m-%d_%H-%M")
  $debug_log = File.open("log/debug-#{timestamp}.log", "w")
  $report_log = File.open("log/report-#{timestamp}.log", "w")
end

# This adds authors with multiple email addresses (found in the
# sourcefile copyrights and hg commit logs).  We want to conflate them
# so we can properly update copyrights.
#
# When a copyright doesn't have an email address listed, the name is
# used as the hash/lookup key.  This is why some of the entries below
# has strange values for the "email address" parameter.
def preseed_authors()
  edmund = Author.new("Edmund Grimley Evans", "edmundo@rano.org")
  edmund.add_email("edmund820@rano.org")
  Author.add_author(edmund)

  g10 = Author.new("g10 Code GmbH", "g10 code gmbh")
  g10.add_email("g10code gmbh")
  g10.add_email("moritz@g10code.com")
  g10.add_email("Moritz.Schulte@ruhr-uni-bochum.de")
  Author.add_author(g10)

  me = Author.new("Michael R. Elkins", "me@cs.hmc.edu")
  me.add_email("me@mutt.org")
  me.add_email("me@sigpipe.org")
  me.add_email("michael r. elkins.")  # this is from filter.c
  Author.add_author(me)

  mikes = Author.new("Mike Schiraldi", "raldi@research.netsol.com")
  mikes.add_email("1074468571@schiraldi.org")
  Author.add_author(mikes)

  rocco = Author.new("Rocco Rutte", "pdmef@gmx.net")
  rocco.add_email("s1118644@mail.inf.tu-dresden.de")
  Author.add_author(rocco)

  werner = Author.new("Werner Koch", "werner.koch@guug.de")
  werner.add_email("wk@gnupg.org")
  werner.add_email("wk@isil.d.shuttle.de")
  Author.add_author(werner)
end

# This creates entries in the Author and SourceFile class
# holding all the files and authors with copyright lines
def scan_source_files(hgdir)
  ["", "imap/"].each do |suffix|
    Dir.foreach("#{hgdir}#{suffix}") do |file|
      next if file !~ /.+\.([ch]|pl)$/
      next if !File.file?("#{hgdir}#{suffix}#{file}")

      source_file = SourceFile.new(hgdir, "#{suffix}#{file}")
      source_file.parse_file()
      if source_file.lines().size() > 0
        SourceFile.add_file(source_file)
      end
    end
  end
end

def init(hgdir)
  open_logs()
  preseed_authors()
  scan_source_files(hgdir)
end

# Silly way to clear the screen, but this will make it easier
# to review things
def clear_screen()
  system("clear")
end

def update_file(source_file)
  # return if !source_file.modified?()

  temp_file = Tempfile.new("updcpr")
  begin
    # dump the copyright updates to the tempfile
    source_file.force_output_file(temp_file)
    temp_file.close()

    perms = File.stat(source_file.path()).mode()

    # copy the temp file over the orginal file, preserving permissions
    File.open(temp_file.path()) do |tempf|
      File.open(source_file.path(), 'wb', perms) do |srcf|
        IO.copy_stream(tempf, srcf)
      end
    end

  ensure
    temp_file.unlink()
  end
end

def update_files()
  SourceFile.source_files().each() do |file|
    update_file(file)
  end

  # update_file($copyright_file)
end

def cleanup()
  $debug_log.close()
  $report_log.close()
end


if ARGV.length != 1
  puts "Usage #{$0} hgrepos"
  puts "hgrepos is the path to the mutt hg repository to update"
  exit 1
end

hgdir = ARGV[0]
if (! hgdir.end_with?("/"))
  hgdir = hgdir + "/"
end
if ((! File.directory?(hgdir)) || (! File.directory?("#{hgdir}/.hg")))
  puts "#{hgdir} doesn't seem to point to a mutt hg repository"
  exit 1
end

init(hgdir)

update_files()

cleanup()



